# Setup on new AWS AMI 2
```
sudo yum update -y
```
```
sudo yum install git
```
```
git clone git@gitlab.com:cliff3/freelancer-com-aws-lambda-containers.git
```
```
sudo amazon-linux-extras install docker
```
```
sudo service docker start
```
```
sudo usermod -a -G docker ec2-user
```
```
pip3 install aws-sam-cli
```
```
aws configure
```
// create swap to prevent low on RAM
```
sudo dd if=/dev/zero of=/swapfile bs=128M count=32
```
```
sudo chmod 600 /swapfile
```
```
sudo mkswap /swapfile
```
```
sudo swapon /swapfile
```
// end of creating swap to prevent low on RAM

# Setting AWS IAM Permission

The IAM user needs:
- IAMFullAccess
- AmazonElastiCacheFullAccess
- AmazonEC2ContainerRegistryFullAccess
- AmazonS3FullAccess
- AmazonAPIGatewayInvokeFullAccess
- AmazonAPIGatewayAdministrator
- AWSCloudFormationFullAccess
- AmazonElasticContainerRegistryPublicFullAccess
- AWSLambda_FullAccess

# First deploy
docker build will take a long time to build images
```
sudo docker build -t phpmyfunction .
```
```
sam build
```
```
sam deploy -g
```

- For Stack Name, enter my-demo.
- Choose the same Region that you created the ECR repository in.
- Confirm changes before deploy: N
- Allow SAM CLI IAM role creation: Y
- Disable rollback: N
- For HelloWorldFunction may not have authorization defined, Is this okay? Y.
- Save arguments to configuration file: Y
- SAM configuration file: keep default
- SAM configuration environment: keep default
- Create managed ECR repositories for all functions?: Y

CLI will display the api end point e.g. https://8cxjmqexh5.execute-api.ap-east-1.amazonaws.com/

# After first deploy
```
sam build
```
```
sam deploy
```

# Sam set up
```
sam init
```
- Which template source would you like to use?:  AWS Quick Start Templates
- What package type would you like to use?: Image
- Choose base image
- enter project name
- change directory to the project root
```
sam build
```
```
sam deploy -g
```
- Enter Stack Name
- Choose the same Region that you created the ECR repository in.
- Confirm changes before deploy: N
- Allow SAM CLI IAM role creation: Y
- For HelloWorldFunction may not have authorization defined, Is this okay? Y.
- Save arguments to configuration file: Y
- SAM configuration file: keep default
- SAM configuration environment: keep default
- Create managed ECR repositories for all functions?: Y
